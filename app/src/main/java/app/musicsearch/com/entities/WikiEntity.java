package app.musicsearch.com.entities;

import app.musicsearch.com.lastfm.rest.api.track.info.Wiki;

public class WikiEntity {

    private String published;
    private String summary;
    private String content;

    public WikiEntity() {}

    public WikiEntity(Wiki wiki) {
        this.published = wiki.getPublished();
        this.summary = wiki.getSummary();
        this.content = wiki.getContent();
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
