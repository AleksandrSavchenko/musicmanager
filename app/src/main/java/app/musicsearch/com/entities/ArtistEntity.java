package app.musicsearch.com.entities;

import app.musicsearch.com.lastfm.rest.api.track.info.Artist;

public class ArtistEntity extends  BaseEntity{

    public ArtistEntity() {
    }

    public ArtistEntity(Artist art) {
        this.setName(art.getName());
        this.setUrl(art.getUrl());
        this.setMbid(art.getMbid());
    }
}
