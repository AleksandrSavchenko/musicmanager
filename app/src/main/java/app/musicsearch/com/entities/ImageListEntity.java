package app.musicsearch.com.entities;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.lastfm.rest.dto.Image;
import app.musicsearch.com.lastfm.rest.dto.ImageType;

public class ImageListEntity {

    List<ImageEntity> images;

    public ImageListEntity(List<Image> list) {
        images = new ArrayList<ImageEntity>();
        for (Image im : list) {
            ImageEntity img = new ImageEntity(im);
            images.add(img);
        }
    }

    public List<ImageEntity> getImages() {
        return images;
    }

    public void setImages(List<ImageEntity> images) {
        this.images = images;
    }

    public String getUrl(ImageType type) {
        for (ImageEntity img : images) {
            if (img.getType() == type)
                return img.getUrl();
        }
        return "";
    }
}
