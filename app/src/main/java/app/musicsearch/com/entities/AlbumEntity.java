package app.musicsearch.com.entities;

import app.musicsearch.com.lastfm.rest.api.track.info.Album;

public class AlbumEntity {
    private String name;
    private String artist;
    private ImageListEntity images;

    public AlbumEntity() {}

    public AlbumEntity(Album alb) {
        this.artist = alb.getArtist();
        this.images = new ImageListEntity(alb.getImage());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public ImageListEntity getImages() {
        return images;
    }

    public void setImages(ImageListEntity images) {
        this.images = images;
    }
}
