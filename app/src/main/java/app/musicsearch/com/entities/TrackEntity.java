package app.musicsearch.com.entities;

import java.util.List;

import app.musicsearch.com.lastfm.rest.api.track.info.Tag;
import app.musicsearch.com.lastfm.rest.api.track.info.TrackInfo;
import app.musicsearch.com.services.ServicesException;
import app.musicsearch.com.services.TrackService;

public class TrackEntity extends BaseEntity {

    private String id;
    private String listeners;
    private String trackStyle;
    private WikiEntity wiki = new WikiEntity();
    private ImageListEntity images;
    private AlbumEntity album = new AlbumEntity();
    private ArtistEntity artist = new ArtistEntity();
    private boolean isLoadFull = false;

    public TrackEntity() {
        this.album = new AlbumEntity();
        this.artist = new ArtistEntity();
        this.wiki = new WikiEntity();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getListeners() {
        return listeners;
    }

    public void setListeners(String listeners) {
        this.listeners = listeners;
    }

    public String getTrackStyle() {
        return trackStyle;
    }

    public void setTrackStyle(String trackStyle) {
        this.trackStyle = trackStyle;
    }

    public WikiEntity getWiki() {
        return wiki;
    }

    public void setWiki(WikiEntity wiki) {
        this.wiki = wiki;
    }

    public AlbumEntity getAlbum() {
        return album;
    }

    public void setAlbum(AlbumEntity album) {
        this.album = album;
    }

    public ImageListEntity getImages() {
        return images;
    }

    public void setImages(ImageListEntity images) {
        this.images = images;
    }

    public ArtistEntity getArtist() {
        return artist;
    }

    public void setArtist(ArtistEntity artist) {
        this.artist = artist;
    }

    public void loadInfo() {

        if (isLoadFull == true) return;

        try {
            TrackInfo info = TrackService.loadInfo(this.getMbid());

            this.setArtist(new ArtistEntity(info.getTrack().getArtist()));
            this.setWiki(new WikiEntity(info.getTrack().getWiki()));
            this.setAlbum(new AlbumEntity(info.getTrack().getAlbum()));
            this.setTrackStyle(toTag(info.getTrack().getToptags().getTag()));
            this.getAlbum().setName(info.getTrack().getAlbum().getTitle());

            isLoadFull = true;

        } catch (ServicesException e) {
            isLoadFull = false;
        }
    }

    private String toTag(List<Tag> tags) {
        String res = "";
        for (Tag t : tags) {
            res += t.getName()+"/";
        }
        return res;
    }
}
