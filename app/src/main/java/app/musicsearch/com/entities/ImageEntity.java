package app.musicsearch.com.entities;

import java.util.List;

import app.musicsearch.com.lastfm.rest.dto.Image;
import app.musicsearch.com.lastfm.rest.dto.ImageType;

public class ImageEntity {

    private ImageType type;
    private String url;

    public ImageEntity(Image img) {
        this.type = ImageType.createType(img.getSize());
        this.url = img.getText();
    }

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
