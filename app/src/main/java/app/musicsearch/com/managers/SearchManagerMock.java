package app.musicsearch.com.managers;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.entities.AlbumEntity;
import app.musicsearch.com.entities.ArtistEntity;
import app.musicsearch.com.entities.ImageListEntity;
import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.lastfm.rest.dto.Image;
import app.musicsearch.com.lastfm.rest.dto.ImageType;

public class SearchManagerMock {
    private int page = 0;
    private int allPage = 0;
    private int countOnPage = 15;
    private int countAll = 0;
    private String textSearch;

    private List<TrackEntity> list;

    public SearchManagerMock() {
        list = new ArrayList<TrackEntity>();
        list.add(create("Artist 1", "Track 1"));
        list.add(create("Artist 2", "Track 2"));
        list.add(create("Artist 3", "Track 3"));
        list.add(create("Artist 4", "Track 4"));
        list.add(create("Artist 5", "Track 5"));
        list.add(create("Artist 6", "Track 6"));
    }

    public void sarch(String trackName) {

    }

    public void nextPage() {
    }

    public void previewPage() {
    }

    public List<TrackEntity> getTracks() {
        return list;
    }

    private TrackEntity create(String artist, String track) {
        TrackEntity entity = new TrackEntity();
        entity.setName(track);
        entity.setAlbum(new AlbumEntity());
        entity.setArtist(new ArtistEntity());
        entity.getArtist().setName(artist);
        entity.getAlbum().setArtist(artist);
        List<Image> list = new ArrayList<Image>();
        list.add(createIMG(ImageType.SMALL));
        list.add(createIMG(ImageType.EXTRALARGE));
        list.add(createIMG(ImageType.LARGE));
        list.add(createIMG(ImageType.MEDIUM));
        entity.setImages(new ImageListEntity(list));
        return entity;
    }

    private Image createIMG(ImageType type) {
        Image im = new Image();
        im.setSize(type.getType());
        im.setText("http://images.mob.org/pic/gallery/240x320/fon-muzyka-15734.jpg");
        return im;
    }

}
