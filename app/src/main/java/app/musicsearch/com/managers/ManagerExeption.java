package app.musicsearch.com.managers;

public class ManagerExeption extends Exception {
    private String message;

    public ManagerExeption(String msg) {
        this.message = msg;
    }

    public String toString() {
        return message;
    }
}
