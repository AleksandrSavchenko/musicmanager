package app.musicsearch.com.managers;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.services.ServicesException;
import app.musicsearch.com.services.TrackService;

public class SearchManager {

    private static int DEFAULT_PAGE = 1;
    private static int DEFAULT_ALL_PAGES = 0;
    private static int DEFAULT_COUNT_ON_PAGE = 10;
    private static int DEFAULT_COUNT_ALL = 0;


    private int page = DEFAULT_PAGE;
    private int allPage = DEFAULT_ALL_PAGES;
    private int countOnPage = DEFAULT_COUNT_ON_PAGE;
    private int countAll = DEFAULT_COUNT_ALL;
    private String textSearch;

    private List<TrackEntity> list;

    public SearchManager() {
        init();
    }

    private void init() {
        page = DEFAULT_PAGE;
        allPage = DEFAULT_ALL_PAGES;
        countOnPage = DEFAULT_COUNT_ON_PAGE;
        countAll = DEFAULT_COUNT_ALL;
        textSearch = "";
        list = new ArrayList<TrackEntity>();
    }

    public void refresh() {
        init();
    }

    public void sarch(String trackName) throws ManagerExeption {
        try {
            textSearch = filtreName(trackName);
            list = TrackService.searchTracks(textSearch, page, countOnPage);
            countAll = TrackService.getAllTracks();
            initProperties();
        } catch (ServicesException e) {
            throw new ManagerExeption("Error accessing the service, please check the connection.");
        }
    }

    public void nextPage() {
        if (page + 1 <= allPage) {
            page++;
        }
    }

    public void previewPage() {
        if (page - 1 > 0) {
            page--;
        }
    }

    public int getCountPages() {
        return allPage;
    }

    public int getPageNumber() {
        return page;
    }

    private String filtreName(String trackName) {
        return trackName.replace(' ', '+');
    }

    public List<TrackEntity> getTracks() {
        return list;
    }

    private void initProperties() {
        allPage = countAll / countOnPage;
        if (countAll % countOnPage != 0) {
            allPage++;
        }
    }

}
