package app.musicsearch.com.musicmanager.datatransfer;

public enum TrackTransfer {

    MBID("mbid"),
    ARTIST("artist"),
    TRACK("track"),
    ALBUM("album"),
    TYPE("type"),
    LINK("link"),
    SUMMARY("summary"),
    IMAGE("image");

    private String type;

    private TrackTransfer(String type) {
        this.type = type;
    }

    public String value(){
        return type;
    }
}
