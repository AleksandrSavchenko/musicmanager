package app.musicsearch.com.musicmanager;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import app.musicsearch.com.database.DataBaseManager;
import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.musicmanager.adapters.LibraryListAdapter;


public class LibraryActivity extends ActionBarActivity {

    private DataBaseManager dbManager;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        dbManager = new DataBaseManager(this);
        listView = (ListView) this.findViewById(R.id.listLibraryView);
        listView.setAdapter(new LibraryListAdapter(this, dbManager));
        new DataBaseLoadTask().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_library, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DataBaseLoadTask extends AsyncTask<Void, Void, List<TrackEntity>> {
        @Override
        protected List<TrackEntity> doInBackground(Void... params) {
            return dbManager.loadTracks();
        }

        @Override
        protected void onPostExecute(List<TrackEntity> result) {
            LibraryListAdapter adapter = (LibraryListAdapter)listView.getAdapter();
            adapter.updateList(result);
            adapter.notifyDataSetChanged();
        }
    }
}
