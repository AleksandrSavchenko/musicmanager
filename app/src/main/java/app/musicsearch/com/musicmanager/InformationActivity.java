package app.musicsearch.com.musicmanager;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import app.musicsearch.com.database.DataBaseManager;
import app.musicsearch.com.musicmanager.datatransfer.TrackInformation;
import app.musicsearch.com.musicmanager.datatransfer.TrackTransfer;
import app.musicsearch.com.musicmanager.tasks.ImageDownloaderTask;


public class InformationActivity extends ActionBarActivity {

    DataBaseManager manager;
    Button btnAdd;

    TrackInformation information;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        information = new TrackInformation();

        loadInfo();
        manager = new DataBaseManager(this);

        btnAdd = (Button) this.findViewById(R.id.btnAddTrack);
        btnAdd.setClickable(false);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.addTrack(information.getMbid(),information.getArtist(),
                        information.getTrack(),information.getAlbum(),information.getType(),
                        information.getLink(),information.getSummary());
                btnAdd.setVisibility(View.GONE);
            }
        });

        new LoadInfoFromDBTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadInfo() {

        Intent intent = getIntent();

        loadTrackInformation(intent);

        TextView artistTV = (TextView) findViewById(R.id.textArtist);
        TextView trackTV = (TextView) findViewById(R.id.textTrack);
        TextView albumTV = (TextView) findViewById(R.id.textAlbum);
        TextView typeTV = (TextView) findViewById(R.id.textMusicType);
        TextView summaryTV = (TextView) findViewById(R.id.textMusicDescription);
        ImageView image = (ImageView) findViewById(R.id.imageView);

        artistTV.setText(information.getArtist());
        trackTV.setText(information.getTrack());
        albumTV.setText(information.getAlbum());
        typeTV.setText(information.getType());
        summaryTV.setText(Html.fromHtml(String.format("<a href=\"%s\">Information</a> ",information.getLink())));
        summaryTV.setMovementMethod(LinkMovementMethod.getInstance());

        new ImageDownloaderTask(image).execute(information.getImageURL());
    }

    private void loadTrackInformation(Intent intent) {
        information = new TrackInformation();

        information.setMbid(intent.getStringExtra(TrackTransfer.MBID.value()));
        information.setArtist(intent.getStringExtra(TrackTransfer.ARTIST.value()));
        information.setTrack(intent.getStringExtra(TrackTransfer.TRACK.value()));
        information.setAlbum(intent.getStringExtra(TrackTransfer.ALBUM.value()));
        information.setType(intent.getStringExtra(TrackTransfer.TYPE.value()));
        information.setLink(intent.getStringExtra(TrackTransfer.LINK.value()));
        information.setSummary(intent.getStringExtra(TrackTransfer.SUMMARY.value()));
        information.setImageURL(intent.getStringExtra(TrackTransfer.IMAGE.value()));

    }

    private class LoadInfoFromDBTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            return manager.checkExist(information.getMbid());
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                btnAdd.setVisibility(View.GONE);
            } else {
                btnAdd.setClickable(true);
            }
        }
    }

}
