package app.musicsearch.com.musicmanager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.lastfm.rest.dto.ImageType;
import app.musicsearch.com.managers.ManagerExeption;
import app.musicsearch.com.managers.SearchManager;
import app.musicsearch.com.musicmanager.adapters.CustomListAdapter;
import app.musicsearch.com.musicmanager.datatransfer.TrackTransfer;


public class SearchActivity extends ActionBarActivity {

    List<TrackEntity> listTrack;
    SearchManager manager;
    Button searchBtn;
    Button btnNext;
    Button btnBack;
    ListView listView;
    Intent intentInfo;
    AlertDialog.Builder dialogBuilder;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        manager = new SearchManager();
        dialogBuilder = new AlertDialog.Builder(this);
        updatePages();
        intentInfo = new Intent(this, InformationActivity.class);

        listTrack = new ArrayList<TrackEntity>();
        listView = (ListView) this.findViewById(R.id.listViewMusic);
        listView.setAdapter(new CustomListAdapter(this,manager.getTracks()));

        searchBtn = (Button) this.findViewById(R.id.SearcTrackButtpn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingBar();
                manager.refresh();
                new HttpSearchRequestTask().execute();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                loadInformation(position);
            }
        });

        btnNext = (Button) this.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingBar();
                manager.nextPage();
                updatePages();
                new HttpSearchRequestTask().execute();
            }
        });

        btnBack = (Button) this.findViewById(R.id.btnPreview);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingBar();
                manager.previewPage();
                updatePages();
                new HttpSearchRequestTask().execute();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpSearchRequestTask extends AsyncTask<Void, Void, List<TrackEntity>> {
        @Override
        protected List<TrackEntity> doInBackground(Void... params) {
            EditText searchText = (EditText) findViewById(R.id.editText);
            try {
                manager.sarch(searchText.getText().toString());
            } catch (ManagerExeption managerExeption) {
                AlertDialog ad = dialogBuilder.create();
                ad.setMessage(managerExeption.toString());
                ad.show();
            }
            return manager.getTracks();
        }

        @Override
        protected void onPostExecute(List<TrackEntity> result) {
            CustomListAdapter adapter = (CustomListAdapter)listView.getAdapter();
            adapter.updateList(result);
            adapter.notifyDataSetChanged();
            updatePages();
            hideLoadingBar();
        }
    }

    private void loadInformation(int index) {
        showLoadingBar();
        new LoadInfoTask().execute(index);
    }

    private void updatePages() {
        TextView txtPages = (TextView) findViewById(R.id.textPages);
        if (manager.getCountPages() != 0) {
            txtPages.setText(String.format("%s/%s", manager.getPageNumber(), manager.getCountPages()));
        } else {
            txtPages.setText("");
        }
    }

    private class LoadInfoTask extends AsyncTask<Integer, Void, TrackEntity> {

        @Override
        protected TrackEntity doInBackground(Integer... params) {
            Integer index = params[0];
            TrackEntity track = (TrackEntity) listView.getItemAtPosition(index);
            track.loadInfo();
            return track;
        }

        @Override
        protected void onPostExecute(TrackEntity result) {
            intentInfo.putExtra(TrackTransfer.MBID.value(), result.getMbid());
            intentInfo.putExtra(TrackTransfer.ARTIST.value(), result.getArtist().getName());
            intentInfo.putExtra(TrackTransfer.TRACK.value(), result.getName());
            intentInfo.putExtra(TrackTransfer.ALBUM.value(), result.getAlbum().getName());
            intentInfo.putExtra(TrackTransfer.TYPE.value(), result.getTrackStyle());
            intentInfo.putExtra(TrackTransfer.LINK.value(), result.getUrl());
            intentInfo.putExtra(TrackTransfer.SUMMARY.value(), result.getWiki().getSummary());
            intentInfo.putExtra(TrackTransfer.IMAGE.value(), result.getImages().getUrl(ImageType.EXTRALARGE));
            hideLoadingBar();
            startActivity(intentInfo);
        }
    }

    public void showLoadingBar()
    {
        dialog = ProgressDialog.show(SearchActivity.this,"","Loading...");
    }
    public void hideLoadingBar()
    {
        dialog.dismiss();

    }


}
