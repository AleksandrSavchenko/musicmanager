package app.musicsearch.com.musicmanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.database.DataBaseManager;
import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.musicmanager.R;

public class LibraryListAdapter extends BaseAdapter {

    List<TrackEntity> listTrack;
    private Activity context;
    LayoutInflater inflater;
    private DataBaseManager dbManager;

    public LibraryListAdapter(Activity context,DataBaseManager dbManager) {
        listTrack = new ArrayList<TrackEntity>();
        this.context = context;
        this.dbManager = dbManager;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listTrack.size();
    }

    @Override
    public Object getItem(int position) {
        return listTrack.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if(convertView == null)
            vi = inflater.inflate(R.layout.library_row, null);

        TextView track  = (TextView) vi.findViewById(R.id.libtrackName);
        TextView artist = (TextView) vi.findViewById(R.id.libartistName);
        Button btnDel = (Button) vi.findViewById(R.id.btnDel);
        track.setText(listTrack.get(position).getName());
        artist.setText(listTrack.get(position).getArtist().getName());
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mbid = listTrack.get(position).getMbid();
                dbManager.deleteTrack(mbid);
                removeAndUpdate(position);
            }
        });
        return  vi;
    }

    public void updateList(List<TrackEntity> list) {
        this.listTrack = list;
    }

    private void removeAndUpdate(int position) {
        listTrack.remove(position);
        this.notifyDataSetChanged();
    }
}
