package app.musicsearch.com.musicmanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.lastfm.rest.dto.ImageType;
import app.musicsearch.com.musicmanager.R;
import app.musicsearch.com.musicmanager.tasks.ImageDownloaderTask;

public class CustomListAdapter extends BaseAdapter {

    private final Activity context;
    List<TrackEntity> listTrack;
    private static LayoutInflater inflater=null;

    public CustomListAdapter(Activity context, List<TrackEntity> listTrack) {
        this.listTrack = listTrack;
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return listTrack.size();
    }

    public Object getItem(int position) {
        return listTrack.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        View vi=view;
        if(view==null)
            vi = inflater.inflate(R.layout.list_row, null);

        TextView track  = (TextView) vi.findViewById(R.id.trackName);
        TextView artist = (TextView) vi.findViewById(R.id.artistName);
        ImageView image = (ImageView) vi.findViewById(R.id.albumImage);

        track.setText(listTrack.get(position).getName());
        artist.setText("");
        if (listTrack.get(position).getArtist() != null) {
            artist.setText(listTrack.get(position).getArtist().getName());
        }

        if ( image != null) {
            String url = listTrack.get(position).getImages().getUrl(ImageType.MEDIUM);
            new ImageDownloaderTask(image).execute(url);
        }

        return vi;
    };

    public void updateList(List<TrackEntity> list) {
        this.listTrack = list;
    }
}
