package app.musicsearch.com.services;

import app.musicsearch.com.entities.ArtistEntity;
import app.musicsearch.com.entities.ImageListEntity;
import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.lastfm.rest.dto.Track;

public class Converter {

    public static TrackEntity convert(Track track) {
        TrackEntity result = new TrackEntity();
        result.setMbid(track.getMbid());
        result.setName(track.getName());
        result.setUrl(track.getUrl());
        result.setImages(new ImageListEntity(track.getImage()));
        ArtistEntity artistEntity = new ArtistEntity();
        artistEntity.setName(track.getArtist());
        result.setArtist(artistEntity);

        return result;
    }
}
