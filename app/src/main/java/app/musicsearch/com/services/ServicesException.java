package app.musicsearch.com.services;

public class ServicesException extends Exception {
    private String msg;

    public ServicesException(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return msg;
    }
}
