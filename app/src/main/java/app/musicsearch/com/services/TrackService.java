package app.musicsearch.com.services;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.lastfm.rest.api.exception.ApiRESTException;
import app.musicsearch.com.lastfm.rest.api.track.ApiManagerTrack;
import app.musicsearch.com.lastfm.rest.api.track.info.TrackInfo;
import app.musicsearch.com.lastfm.rest.dto.Track;

public class TrackService {

    private static String API_KEY = "18f51912e105e6614fd4e8a7ea0067bf";
    private static ApiManagerTrack manager;

    static {
        manager = new ApiManagerTrack(API_KEY);
    }

    public static List<TrackEntity> searchTracks(String trackName, int page, int countOnPage) throws ServicesException {
        int httpCode = 0;
        try {
            httpCode = manager.searchTrack(trackName, page, countOnPage);
        } catch (ApiRESTException e) {
            throw new ServicesException("When searching for the error occurred.");
        }

        if (httpCode != 200) {
            throw new ServicesException("Http connection error");
        }

        List<TrackEntity> result = new ArrayList<TrackEntity>();

        for (Track tr : manager.getTracksResult()) {
            result.add(Converter.convert(tr));
        }

        return result;
    }

    public static TrackInfo loadInfo(String trackID) throws ServicesException {
        int httpCode = 0;
        try {
            httpCode = manager.getTrackInfo(trackID);
        } catch (ApiRESTException e) {
            throw new ServicesException("When load track info for the error occurred.");
        }

        if (httpCode != 200) {
            throw new ServicesException("Http connection error");
        }

        return manager.getLastInfo();
    }

    public static int getItemsPerPage() {
        return manager.getItemsPerPage();
    }

    public static int getPage() {
        return manager.getPage();
    }

    public static int getAllTracks() {
        return manager.getAllTracks();
    }

    public static int getAllPages() {
        int all = getAllTracks();
        int onPage = getItemsPerPage();
        int res =  all % onPage == 0 ? all / onPage : all / onPage + 1;
        return res;
    }
}
