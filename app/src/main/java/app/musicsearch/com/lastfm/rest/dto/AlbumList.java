package app.musicsearch.com.lastfm.rest.dto;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;

public class AlbumList {
	@JsonProperty("album")
	private ArrayList<Album> albumList;
	
	public AlbumList(){
		albumList = new ArrayList<Album>();
	}
	
	public AlbumList(ArrayList<Album> album){
		albumList = album;
	}

	public ArrayList<Album> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(ArrayList<Album> albumList) {
		this.albumList = albumList;
	}
	
	public Album get(int index){
		if (albumList == null) 
			return null;
		if (index < 0 || index>albumList.size())
			return null;
		return albumList.get(index);
	}
	
	public void add(Album album){
		if (albumList!=null)
		this.albumList.add(album);
	}
}
