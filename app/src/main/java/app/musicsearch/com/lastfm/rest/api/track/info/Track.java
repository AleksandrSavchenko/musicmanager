package app.musicsearch.com.lastfm.rest.api.track.info;

import org.codehaus.jackson.annotate.JsonProperty;

import app.musicsearch.com.lastfm.rest.dto.Streamable;

public class Track {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mbid")
    private String mbid;
    @JsonProperty("url")
    private String url;
    @JsonProperty("duration")
    private String duration;
    @JsonProperty("streamable")
    private Streamable streamable;
    @JsonProperty("listeners")
    private String listeners;
    @JsonProperty("playcount")
    private String playcount;
    @JsonProperty("artist")
    private Artist artist;
    @JsonProperty("album")
    private Album album;
    @JsonProperty("toptags")
    private Toptags toptags;
    @JsonProperty("wiki")
    private Wiki wiki;

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The mbid
     */
    @JsonProperty("mbid")
    public String getMbid() {
        return mbid;
    }

    /**
     * 
     * @param mbid
     *     The mbid
     */
    @JsonProperty("mbid")
    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public String getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The streamable
     */
    @JsonProperty("streamable")
    public Streamable getStreamable() {
        return streamable;
    }

    /**
     * 
     * @param streamable
     *     The streamable
     */
    @JsonProperty("streamable")
    public void setStreamable(Streamable streamable) {
        this.streamable = streamable;
    }

    /**
     * 
     * @return
     *     The listeners
     */
    @JsonProperty("listeners")
    public String getListeners() {
        return listeners;
    }

    /**
     * 
     * @param listeners
     *     The listeners
     */
    @JsonProperty("listeners")
    public void setListeners(String listeners) {
        this.listeners = listeners;
    }

    /**
     * 
     * @return
     *     The playcount
     */
    @JsonProperty("playcount")
    public String getPlaycount() {
        return playcount;
    }

    /**
     * 
     * @param playcount
     *     The playcount
     */
    @JsonProperty("playcount")
    public void setPlaycount(String playcount) {
        this.playcount = playcount;
    }

    /**
     * 
     * @return
     *     The artist
     */
    @JsonProperty("artist")
    public Artist getArtist() {
        return artist;
    }

    /**
     * 
     * @param artist
     *     The artist
     */
    @JsonProperty("artist")
    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    /**
     * 
     * @return
     *     The album
     */
    @JsonProperty("album")
    public Album getAlbum() {
        return album;
    }

    /**
     * 
     * @param album
     *     The album
     */
    @JsonProperty("album")
    public void setAlbum(Album album) {
        this.album = album;
    }

    /**
     * 
     * @return
     *     The toptags
     */
    @JsonProperty("toptags")
    public Toptags getToptags() {
        return toptags;
    }

    /**
     * 
     * @param toptags
     *     The toptags
     */
    @JsonProperty("toptags")
    public void setToptags(Toptags toptags) {
        this.toptags = toptags;
    }

    /**
     * 
     * @return
     *     The wiki
     */
    @JsonProperty("wiki")
    public Wiki getWiki() {
        return wiki;
    }

    /**
     * 
     * @param wiki
     *     The wiki
     */
    @JsonProperty("wiki")
    public void setWiki(Wiki wiki) {
        this.wiki = wiki;
    }

}
