package app.musicsearch.com.lastfm.rest.api.track.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import app.musicsearch.com.lastfm.rest.dto.Image;

public class Album {

    @JsonProperty("artist")
    private String artist;
    @JsonProperty("title")
    private String title;
    @JsonProperty("mbid")
    private String mbid;
    @JsonProperty("url")
    private String url;
    @JsonProperty("image")
    private List<Image> image = new ArrayList<Image>();
    @JsonProperty("@attr")
    private Attr Attr;

    /**
     * 
     * @return
     *     The artist
     */
    @JsonProperty("artist")
    public String getArtist() {
        return artist;
    }

    /**
     * 
     * @param artist
     *     The artist
     */
    @JsonProperty("artist")
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The mbid
     */
    @JsonProperty("mbid")
    public String getMbid() {
        return mbid;
    }

    /**
     * 
     * @param mbid
     *     The mbid
     */
    @JsonProperty("mbid")
    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public List<Image> getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(List<Image> image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The Attr
     */
    @JsonProperty("@attr")
    public Attr getAttr() {
        return Attr;
    }

    /**
     * 
     * @param Attr
     *     The @attr
     */
    @JsonProperty("@attr")
    public void setAttr(Attr Attr) {
        this.Attr = Attr;
    }

}
