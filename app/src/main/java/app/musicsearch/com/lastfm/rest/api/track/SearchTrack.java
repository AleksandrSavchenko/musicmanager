
package app.musicsearch.com.lastfm.rest.api.track;

import org.codehaus.jackson.annotate.JsonProperty;

public class SearchTrack {

    @JsonProperty("results")
    private SearchResults results;

    /**
     * 
     * @return
     *     The results
     */
    @JsonProperty("results")
    public SearchResults getResults() {
        return results;
    }

    /**
     * 
     * @param results
     *     The results
     */
    @JsonProperty("results")
    public void setResults(SearchResults results) {
        this.results = results;
    }

}
