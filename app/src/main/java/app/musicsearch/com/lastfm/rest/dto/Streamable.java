
package app.musicsearch.com.lastfm.rest.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class Streamable {

    @JsonProperty("#text")
    private String Text;
    @JsonProperty("fulltrack")
    private String fulltrack;

    /**
     * 
     * @return
     *     The Text
     */
    @JsonProperty("#text")
    public String getText() {
        return Text;
    }

    /**
     * 
     * @param Text
     *     The #text
     */
    @JsonProperty("#text")
    public void setText(String Text) {
        this.Text = Text;
    }

    /**
     * 
     * @return
     *     The fulltrack
     */
    @JsonProperty("fulltrack")
    public String getFulltrack() {
        return fulltrack;
    }

    /**
     * 
     * @param fulltrack
     *     The fulltrack
     */
    @JsonProperty("fulltrack")
    public void setFulltrack(String fulltrack) {
        this.fulltrack = fulltrack;
    }

}
