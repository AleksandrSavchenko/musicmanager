package app.musicsearch.com.lastfm.rest.api.exception;

public class ApiRESTException extends Exception{

	private static final long serialVersionUID = 984727597842910049L;
	
	private String msg;
	
	public ApiRESTException(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	@Override
	public String toString() {
		return String.format("ApiRESTException [msg=%s]",msg);
	}
	
	public static ApiRESTException create(String msg) {
		return new ApiRESTException(msg);
	}
	
}
