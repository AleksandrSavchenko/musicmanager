package app.musicsearch.com.lastfm.rest.api.track;

import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import app.musicsearch.com.lastfm.rest.api.exception.ApiRESTException;
import app.musicsearch.com.lastfm.rest.api.track.info.TrackInfo;
import app.musicsearch.com.lastfm.rest.dto.Track;

/**
 * Manager to track for last-fm
 * @author AlexKbit
 *
 */
public class ApiManagerTrack {

	private final static String urlTrackSearch = "http://ws.audioscrobbler.com/2.0/?method=track.search&track=%s&page=%s&limit=%s&api_key=%s&format=json";
	private final static String urlTrackInfo = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&mbid=%s&api_key=%s&format=json";
	
	private String api_key;
	private SearchTrack search;
	private TrackInfo lastInfo;
	
	public ApiManagerTrack(String key) {
		this.api_key = key;
	}
	
	/**
	 * Search track from last-fm api
	 * @param trackName name of track
	 * @param pageIndex page index
	 * @param limit limit tracks on page
	 * @return HttpCode
	 * @throws ApiRESTException
	 */
	public int searchTrack(String trackName, int pageIndex, int limit) throws ApiRESTException {
		if (limit < 0) limit = 10;
        if (pageIndex < 0) pageIndex = 0;
		String url = String.format(urlTrackSearch, trackName, Integer.toString(pageIndex), Integer.toString(limit), api_key);

		String result= connect(url);
		search = getSearTrack(result);

		return 200; //OK
	}
	
	/**
	 * Search track from last-fm api (limit on page default = 15)
	 * @param trackName name of track
	 * @param limit limit
	 * @return HttpCode
	 * @throws ApiRESTException
	 */
	public int searchTrack(String trackName, int limit) throws ApiRESTException {
		return searchTrack(trackName, -1, limit);
	}

    /**
     * Search track from last-fm api (limit on page default = 15)
     * @param trackName name of track
     * @return HttpCode
     * @throws ApiRESTException
     */
    public int searchTrack(String trackName) throws ApiRESTException {
        return searchTrack(trackName, -1, -1);
    }
	
	/**
	 * Gets track info by track-id
	 * @param trackID mbId
	 * @return HttpCode
	 * @throws ApiRESTException
	 */
	public int getTrackInfo(String trackID) throws ApiRESTException {
		String url = String.format(urlTrackInfo, trackID, api_key);
		String result= connect(url);
		lastInfo = getInfoTrack(result);

		return 200;
	}
	
	private TrackInfo getInfoTrack(String json) throws ApiRESTException{
		byte[] buf = json.getBytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(buf);
		ObjectMapper mapper = new ObjectMapper();
		TrackInfo trInf = new TrackInfo();
		try {
			trInf = mapper.readValue(stream, TrackInfo.class);
		} catch (JsonParseException e) {
			ApiRESTException.create("JSON parse exception");
		} catch (JsonMappingException e) {
			ApiRESTException.create("JSON mapping exception");
		} catch (IOException e) {
			ApiRESTException.create("Incorect result file");
		}
		return trInf;
	}

	private SearchTrack getSearTrack(String json) throws ApiRESTException{
		byte[] buf = json.getBytes();
		ByteArrayInputStream stream = new ByteArrayInputStream(buf);
		ObjectMapper mapper = new ObjectMapper();
		SearchTrack alb = new SearchTrack();
		try {
			alb = mapper.readValue(stream, SearchTrack.class);
		} catch (JsonParseException e) {
			ApiRESTException.create("JSON parse exception");
		} catch (JsonMappingException e) {
			ApiRESTException.create("JSON mapping exception");
		} catch (IOException e) {
			ApiRESTException.create("Incorect result file");
		}
		return alb;
	}
	
	/**
	 * Gets track list after search
	 * @return List<Track>
	 */
	public List<Track> getTracksResult() {
		return search.getResults().getTracks().getTrack();
	}

	/**
	 * Gets track info after search
	 * @return TrackInfo
	 */
	public TrackInfo getLastInfo() {
		return lastInfo;
	}

    public int getAllTracks() {
        if (search != null) {
            return Integer.parseInt(search.getResults().getTotalResults());
        }
        return 0;
    }

    public int getPage() {
        if (search != null) {
            return Integer.parseInt(search.getResults().getQuery().getStartPage());
        }
        return -1;
    }


    public int getItemsPerPage() {
        if (search != null) {
            return Integer.parseInt(search.getResults().getItemsPerPage());
        }
        return 0;
    }

    public static String connect(String url)
    {
        HttpClient httpclient = new DefaultHttpClient();

        HttpGet httpget = new HttpGet(url);

        HttpResponse response;
        String result= "";
        try {
            response = httpclient.execute(httpget);
            Log.i("Praeda", response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                result= convertStreamToString(instream);
                instream.close();
            }


        } catch (Exception e) {}
        return result;
    }

    /*
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     */
    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
