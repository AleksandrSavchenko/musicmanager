package app.musicsearch.com.lastfm.rest.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class Query {

	@JsonProperty("#text")
	private String text;
	@JsonProperty("role")
	private String role;
	@JsonProperty("searchTerms")
	private String searchTerms;
	@JsonProperty("startPage")
	private String startPage;

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getSearchTerms() {
		return searchTerms;
	}
	public void setSearchTerms(String searchTerms) {
		this.searchTerms = searchTerms;
	}
	public String getStartPage() {
		return startPage;
	}
	public void setStartPage(String startPage) {
		this.startPage = startPage;
	}
	
}
