package app.musicsearch.com.lastfm.rest.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class Attribute {

	@JsonProperty("for")
	private String value;
	
	public Attribute(){
		this.value = "";
	}
	
	public Attribute(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
