package app.musicsearch.com.lastfm.rest.dto;

public enum ImageType {
	SMALL("small"), MEDIUM("medium"), LARGE("large"), EXTRALARGE("extralarge");
	
	private final String type;
	
	private ImageType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	public static ImageType createType(String type) {

		if (SMALL.getType().equals(type)) return ImageType.SMALL;
        if (MEDIUM.getType().equals(type)) return ImageType.MEDIUM;
        if (LARGE.getType().equals(type)) return ImageType.LARGE;
        if (EXTRALARGE.getType().equals(type)) return ImageType.EXTRALARGE;

        return null;
	}
}
