package app.musicsearch.com.lastfm.rest.api.track.info;

import org.codehaus.jackson.annotate.JsonProperty;

public class Attr {

    @JsonProperty("position")
    private String position;

    /**
     * 
     * @return
     *     The position
     */
    @JsonProperty("position")
    public String getPosition() {
        return position;
    }

    /**
     * 
     * @param position
     *     The position
     */
    @JsonProperty("position")
    public void setPosition(String position) {
        this.position = position;
    }
}
