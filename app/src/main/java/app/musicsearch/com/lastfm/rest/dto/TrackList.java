
package app.musicsearch.com.lastfm.rest.dto;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class TrackList {

    @JsonProperty("track")
    private List<Track> track = new ArrayList<Track>();

    
    public List<Track> getTrack() {
        return track;
    }

    
    public void setTrack(List<Track> track) {
        this.track = track;
    }

}
