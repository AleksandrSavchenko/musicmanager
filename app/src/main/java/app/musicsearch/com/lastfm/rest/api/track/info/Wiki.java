package app.musicsearch.com.lastfm.rest.api.track.info;

import org.codehaus.jackson.annotate.JsonProperty;

public class Wiki {

    @JsonProperty("published")
    private String published;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("content")
    private String content;

    /**
     * 
     * @return
     *     The published
     */
    @JsonProperty("published")
    public String getPublished() {
        return published;
    }

    /**
     * 
     * @param published
     *     The published
     */
    @JsonProperty("published")
    public void setPublished(String published) {
        this.published = published;
    }

    /**
     * 
     * @return
     *     The summary
     */
    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    /**
     * 
     * @param summary
     *     The summary
     */
    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * 
     * @return
     *     The content
     */
    @JsonProperty("content")
    public String getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    @JsonProperty("content")
    public void setContent(String content) {
        this.content = content;
    }

}
