
package app.musicsearch.com.lastfm.rest.api.track;

import org.codehaus.jackson.annotate.JsonProperty;
import app.musicsearch.com.lastfm.rest.dto.Attribute;
import app.musicsearch.com.lastfm.rest.dto.Query;
import app.musicsearch.com.lastfm.rest.dto.TrackList;

public class SearchResults {

	@JsonProperty("opensearch:Query")
	private Query query;
	@JsonProperty("opensearch:totalResults")
	private String totalResults;
	@JsonProperty("opensearch:startIndex")
	private String startIndex;
	@JsonProperty("opensearch:itemsPerPage")
	private String itemsPerPage;
	@JsonProperty("trackmatches")
	private TrackList tracks;
	@JsonProperty("@attr")
	private Attribute attribute;
	
	
	public SearchResults(){
		query = new Query();
		tracks = new TrackList();
		attribute = new Attribute();
	}
	
	public Query getQuery() {
		return query;
	}
	public void setQuery(Query query) {
		this.query = query;
	}
	public String getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(String totalResults) {
		this.totalResults = totalResults;
	}
	public String getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(String startIndex) {
		this.startIndex = startIndex;
	}
	public String getItemsPerPage() {
		return itemsPerPage;
	}
	public void setItemsPerPage(String itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}
	public Attribute getAttribute() {
		return attribute;
	}
	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}
	public TrackList getTracks() {
		return tracks;
	}
	public void setTracks(TrackList tracks) {
		this.tracks = tracks;
	}
	
	
}
