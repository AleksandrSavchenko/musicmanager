package app.musicsearch.com.lastfm.rest.dto;

import org.codehaus.jackson.annotate.JsonProperty;


public class Image {
	
	@JsonProperty("#text")
	private String text;
	private String size;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	public ImageType getType() {
		return ImageType.createType(size);
	}

}
