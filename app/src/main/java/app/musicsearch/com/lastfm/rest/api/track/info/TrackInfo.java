package app.musicsearch.com.lastfm.rest.api.track.info;

import org.codehaus.jackson.annotate.JsonProperty;

public class TrackInfo {

    @JsonProperty("track")
    private Track track;

    /**
     * 
     * @return
     *     The track
     */
    @JsonProperty("track")
    public Track getTrack() {
        return track;
    }

    /**
     * 
     * @param track
     *     The track
     */
    @JsonProperty("track")
    public void setTrack(Track track) {
        this.track = track;
    }

}
