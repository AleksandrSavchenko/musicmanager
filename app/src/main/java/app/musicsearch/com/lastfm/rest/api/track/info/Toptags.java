package app.musicsearch.com.lastfm.rest.api.track.info;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class Toptags {

    @JsonProperty("tag")
    private List<Tag> tag = new ArrayList<Tag>();

    /**
     * 
     * @return
     *     The tag
     */
    @JsonProperty("tag")
    public List<Tag> getTag() {
        return tag;
    }

    /**
     * 
     * @param tag
     *     The tag
     */
    @JsonProperty("tag")
    public void setTag(List<Tag> tag) {
        this.tag = tag;
    }

}
