package app.musicsearch.com.lastfm.rest.dto;

import java.util.ArrayList;


public class Album {
	private String name;
	private String artist;
	private String id;
	private String url;

    private ArrayList<Image> image;

	private String streamable;
	private String mbid;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStreamable() {
		return streamable;
	}

	public void setStreamable(String streamable) {
		this.streamable = streamable;
	}

	public String getMbid() {
		return mbid;
	}

	public void setMbid(String mbid) {
		this.mbid = mbid;
	}


	public ArrayList<Image> getImage() {
		return image;
	}

	public void setImage(ArrayList<Image> image) {
		this.image = image;
	}
	

}
