package app.musicsearch.com.database;

import android.content.Context;

import java.util.List;

import app.musicsearch.com.entities.TrackEntity;

public class DataBaseManager {
    private DataBaseHelper dbHelper;

    public DataBaseManager(Context context) {
        dbHelper = new DataBaseHelper(context);
    }

    public void addTrack(TrackEntity track) {
        dbHelper.create(track);
        dbHelper.close();
    }

    public void addTrack(String mbid, String artist, String track, String album, String type, String link, String summary) {
        dbHelper.create(mbid, artist,track,album,type,link,summary);
        dbHelper.close();
    }

    public void deleteTrack(String mbid) {
        dbHelper.delete(mbid);
        dbHelper.close();
    }

    public void updateTrack(TrackEntity track) {
        if (dbHelper.checkExist(track.getMbid())) {
            dbHelper.update(track);
            dbHelper.close();
        }
    }

    public List<TrackEntity> loadTracks() {
        List<TrackEntity> res = dbHelper.getAllTracks();
        dbHelper.close();
        return res;
    }

    public Integer numberOfTracks() {
        int n = dbHelper.numberOfTracks();
        dbHelper.close();
        return n;

    }

    public boolean checkExist(String mbid) {
        boolean res = dbHelper.checkExist(mbid);
        dbHelper.close();
        return res;
    }

}
