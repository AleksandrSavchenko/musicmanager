package app.musicsearch.com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import app.musicsearch.com.entities.ImageListEntity;
import app.musicsearch.com.entities.TrackEntity;
import app.musicsearch.com.lastfm.rest.dto.Image;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MusicLabrary.db";
    public static final String TABLE_TRACK = "Track";

    public static final String CONTACTS_COLUMN_ID = "cid";
    public static final String CONTACTS_COLUMN_MBID = "mbid";
    public static final String CONTACTS_COLUMN_ARTIST = "artist";
    public static final String CONTACTS_COLUMN_TRACK = "track";
    public static final String CONTACTS_COLUMN_ALBUM = "album";
    public static final String CONTACTS_COLUMN_TYPE = "type";
    public static final String CONTACTS_COLUMN_LINK = "link";
    public static final String CONTACTS_COLUMN_SUMMARY = "summary";
    public static final String CONTACTS_COLUMN_IMAGE = "image";

    private static final String CMD_CREATE = String.format("CREATE TABLE IF NOT EXISTS %s(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT);",
            TABLE_TRACK,CONTACTS_COLUMN_ID,CONTACTS_COLUMN_MBID,CONTACTS_COLUMN_ARTIST,CONTACTS_COLUMN_TRACK,
            CONTACTS_COLUMN_ALBUM,CONTACTS_COLUMN_TYPE,CONTACTS_COLUMN_LINK,CONTACTS_COLUMN_SUMMARY,CONTACTS_COLUMN_IMAGE);
    private static final String CMD_DROP = String.format("DROP TABLE IF EXISTS %s", TABLE_TRACK);
    private static final String CMD_SELECT_ALL = String.format("SELECT * FROM %s", TABLE_TRACK);


    public DataBaseHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CMD_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(CMD_DROP);
        onCreate(db);
    }

    public Integer numberOfTracks(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_TRACK);
        return numRows;
    }

    public boolean create(TrackEntity track)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CONTACTS_COLUMN_MBID, track.getMbid());
        contentValues.put(CONTACTS_COLUMN_ARTIST, track.getArtist().getName());
        contentValues.put(CONTACTS_COLUMN_TRACK, track.getName());
        contentValues.put(CONTACTS_COLUMN_ALBUM, track.getAlbum().getName());
        contentValues.put(CONTACTS_COLUMN_TYPE, track.getTrackStyle());
        contentValues.put(CONTACTS_COLUMN_LINK, track.getUrl());
        contentValues.put(CONTACTS_COLUMN_SUMMARY, track.getWiki().getSummary());
        contentValues.put(CONTACTS_COLUMN_IMAGE, convert(track.getImages()));

        db.insert(TABLE_TRACK, null, contentValues);
        return true;
    }

    public boolean create(String mbid, String artist, String track, String album, String type, String link, String summary)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CONTACTS_COLUMN_MBID, mbid);
        contentValues.put(CONTACTS_COLUMN_ARTIST, artist);
        contentValues.put(CONTACTS_COLUMN_TRACK, track);
        contentValues.put(CONTACTS_COLUMN_ALBUM, album);
        contentValues.put(CONTACTS_COLUMN_TYPE, type);
        contentValues.put(CONTACTS_COLUMN_LINK, link);
        contentValues.put(CONTACTS_COLUMN_SUMMARY, summary);

        db.insert(TABLE_TRACK, null, contentValues);
        return true;
    }

    public boolean update(TrackEntity track)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(CONTACTS_COLUMN_MBID, track.getMbid());
        contentValues.put(CONTACTS_COLUMN_ARTIST, track.getArtist().getName());
        contentValues.put(CONTACTS_COLUMN_TRACK, track.getName());
        contentValues.put(CONTACTS_COLUMN_ALBUM, track.getAlbum().getName());
        contentValues.put(CONTACTS_COLUMN_TYPE, track.getTrackStyle());
        contentValues.put(CONTACTS_COLUMN_LINK, track.getUrl());
        contentValues.put(CONTACTS_COLUMN_SUMMARY, track.getWiki().getSummary());
        contentValues.put(CONTACTS_COLUMN_IMAGE, convert(track.getImages()));

        db.update(TABLE_TRACK, contentValues, String.format("%s = ? ", CONTACTS_COLUMN_MBID), new String[]{track.getMbid()});
        return true;
    }

    public Integer delete(String mbid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_TRACK, String.format("%s = ? ",CONTACTS_COLUMN_MBID), new String[] {mbid});
    }

    public boolean checkExist(String mbid) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM Track WHERE mbid = ?", new String[] {mbid});
        return res.moveToFirst();
    }

    private String convert(ImageListEntity images) {
        String result = "";
        result+= images.getImages().get(0).getType().toString()+"|"+images.getImages().get(0).getUrl()+"|";
        result+= images.getImages().get(1).getType().toString()+"|"+images.getImages().get(1).getUrl()+"|";
        result+= images.getImages().get(2).getType().toString()+"|"+images.getImages().get(2).getUrl()+"|";
        result+= images.getImages().get(3).getType().toString()+"|"+images.getImages().get(3).getUrl();
        return  result;
    }

    private ImageListEntity convert(String images) {
        if (images == null) return null;
        String[] result = images.split("|");
        List<Image> list = new ArrayList<Image>();
        for (int i = 0; i<8; i+=2) {
            Image im = new Image();
            im.setSize(result[i+0]);
            im.setSize(result[i+1]);
            list.add(im);
        }
        ImageListEntity listEntity = new ImageListEntity(list);

        return listEntity;
    }

    public ArrayList<TrackEntity> getAllTracks()
    {
        ArrayList<TrackEntity> list = new ArrayList<TrackEntity>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(CMD_SELECT_ALL, null);
        res.moveToFirst();

        while(res.isAfterLast() == false){
            TrackEntity track = new TrackEntity();

            track.setMbid(res.getString(res.getColumnIndex(CONTACTS_COLUMN_MBID)));
            track.getArtist().setName(res.getString(res.getColumnIndex(CONTACTS_COLUMN_ARTIST)));
            track.setName(res.getString(res.getColumnIndex(CONTACTS_COLUMN_TRACK)));
            track.getAlbum().setName(res.getString(res.getColumnIndex(CONTACTS_COLUMN_ALBUM)));
            track.setTrackStyle(res.getString(res.getColumnIndex(CONTACTS_COLUMN_TYPE)));
            track.setUrl(res.getString(res.getColumnIndex(CONTACTS_COLUMN_LINK)));
            track.getWiki().setSummary(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SUMMARY)));
            track.setImages(convert(res.getString(res.getColumnIndex(CONTACTS_COLUMN_IMAGE))));

            list.add(track);
            res.moveToNext();
        }
        return list;
    }
}
