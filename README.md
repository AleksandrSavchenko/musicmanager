# musicManager
This is a android application for search and create library of music
#Application Menu
![alt tag](https://github.com/AlexKbit/musicManager/blob/master/screens/Menu.jpg)
#Search music activity
![alt tag](https://github.com/AlexKbit/musicManager/blob/master/screens/SearchMusic.jpg)
#Information about track from list
![alt tag](https://github.com/AlexKbit/musicManager/blob/master/screens/TrackInfo.jpg)
#Music library from SQLLite
![alt tag](https://github.com/AlexKbit/musicManager/blob/master/screens/MusicLibrary.jpg)
